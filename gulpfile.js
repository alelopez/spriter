var gulp = require('gulp');

var svgSprite = require("gulp-svg-sprites"),
    filter    = require('gulp-filter'),
    svg2png   = require('gulp-svg2png')
;

gulp.task('sprites', function () {
    return gulp.src('images/**/_*.svg')
    .pipe(svgSprite(config))
    .pipe(gulp.dest("sprite")) // Write the sprite-sheet + CSS + Preview
    .pipe(filter("**/*.svg"))  // Filter out everything except the SVG file
    .pipe(svg2png())           // Create a PNG
    .pipe(gulp.dest('sprite'));
});

gulp.task('default', ['sprites'])
